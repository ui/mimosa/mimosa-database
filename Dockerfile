# Stolen from https://github.com/lindycoder/prepopulated-mysql-container-example
FROM mariadb:10.8 as builder

RUN ["sed", "-i", "s/exec \"$@\"/echo \"not running $@\"/", "/usr/local/bin/docker-entrypoint.sh"]

ENV MYSQL_ROOT_PASSWORD=root
ENV MYSQL_PASSWORD=test
ENV MYSQL_USER=test 
ENV MYSQL_DATABASE=test

COPY schemas/mimosa/tables.sql /docker-entrypoint-initdb.d/1_tables.sql
COPY schemas/mimosa/lookups.sql /docker-entrypoint-initdb.d/2_lookups.sql
COPY schemas/mimosa/data.sql /docker-entrypoint-initdb.d/3_data.sql

RUN /usr/local/bin/docker-entrypoint.sh mysqld --datadir /initialized-db --aria-log-dir-path /initialized-db && \
    rm /docker-entrypoint-initdb.d/1_tables.sql /docker-entrypoint-initdb.d/2_lookups.sql /docker-entrypoint-initdb.d/3_data.sql && \
    apt-get clean autoclean && apt-get autoremove --yes && \
    rm -rf /var/lib/apt/lists/*

FROM mariadb:10.8
COPY --from=builder /initialized-db /var/lib/mysql
