image: mariadb:10.8

stages:
  - build
  - regenerate
  - push
  - publish

# docker incompatability with jammy:
# https://serverfault.com/questions/1103333/mariadb-10-8-3-in-docker-cant-initialize-timers
services:
  - mariadb:10.8

variables:
  MYSQL_USER: test
  MYSQL_PASSWORD: test
  MYSQL_DATABASE: mimosa_build
  MYSQL_ROOT_PASSWORD: rootpassword
  
build_db:
  stage: build
  script:
    - cp .my.example.cnf .my.cnf
    - bash build.sh
  only:
    - main

regenerate_sql:
  stage: regenerate
  script:
    - cp .my.example.cnf .my.cnf
    - bash build.sh
    - mysqldump --version
    - cd bin
    - bash backup.sh ../schemas/mimosa/
    - rm ../.my.cnf
  artifacts:
    paths:
      - schemas/
  except:
    - main

create_pr:
  stage: push
  image: alpine
  script:
    - apk update && apk add git
    - CHANGES=$(git status --porcelain | wc -l)
    - |
      if [[ "${CHANGES}" -gt 0 ]]; then
        git config user.email "group_4944_bot_7a09b502b2220d38a145e0fc5d72cb45@noreply.gitlab.esrf.fr"
        git config user.name "group_4944_bot_7a09b502b2220d38a145e0fc5d72cb45"
        git remote remove group_origin || true
        git remote add group_origin "https://oauth2:$GROUP_TOKEN@gitlab.esrf.fr/ui/mimosa/mimosa-database.git"
        git add .
        git checkout -b "update-schema-sql-$CI_COMMIT_SHORT_SHA"
        git commit -m "Update database sql files"
        git push group_origin "update-schema-sql-$CI_COMMIT_SHORT_SHA" -o ci.skip -o merge_request.create -o merge_request.target=$CI_COMMIT_REF_NAME -o merge_request.title="Update database sql files"
      else
        echo "Nothing to commit"
      fi        
  dependencies:
    - regenerate_sql
  except:
    - main

docker:
  stage: publish
  image: docker:latest
  services:
    - docker:24.0.5-dind
  variables:
    DOCKER_IMAGE_FULLNAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - echo date > marker
    - docker build --pull --no-cache -t "$DOCKER_IMAGE_FULLNAME" .
    - docker push "$DOCKER_IMAGE_FULLNAME"
  only:
    - main
    - tags

bump_version:
  stage: build
  image: alpine
  script:
    - |
      if [[ -z "$VERSION" ]]; then
        echo "No VERSION provided"
        exit 1
      fi
    - DATE=$(date '+%Y_%m_%d_%H%M')
    - |
      cat > schemas/mimosa/updates/${DATE}_AdminVar_bump_version.sql <<EOL
      INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('${DATE}_AdminVar_bump_version.sql', 'ONGOING');

      UPDATE AdminVar SET \`value\` = '${VERSION}' WHERE \`name\` = 'schemaVersion';

      UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '${DATE}_AdminVar_bump_version.sql';
      EOL
    - |
      apk update && apk add git
      git config user.email "group_4944_bot_7a09b502b2220d38a145e0fc5d72cb45@noreply.gitlab.esrf.fr"
      git config user.name "group_4944_bot_7a09b502b2220d38a145e0fc5d72cb45"
      git remote remove group_origin || true
      git remote add group_origin "https://oauth2:$GROUP_TOKEN@gitlab.esrf.fr/ui/mimosa/mimosa-database.git"
      git add .
      git checkout -b "bump-version-$CI_COMMIT_SHORT_SHA"
      git commit -m "bump AdminVar to $VERSION"
      git push group_origin "bump-version-$CI_COMMIT_SHORT_SHA" -o merge_request.create -o merge_request.target=$CI_COMMIT_REF_NAME -o merge_request.title="Bump AdminVar to $VERSION"
  when:
    manual
