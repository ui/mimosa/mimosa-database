INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('2024_09_19_1303_AdminVar_bump_version.sql', 'ONGOING');

UPDATE AdminVar SET `value` = '2.1.1' WHERE `name` = 'schemaVersion';

UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '2024_09_19_1303_AdminVar_bump_version.sql';
