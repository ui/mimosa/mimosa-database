INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('2023_08_29_Map_scale_type.sql', 'ONGOING');

ALTER TABLE `XRFFluorescenceMapping`
    ADD `scale` VARCHAR(25) NULL DEFAULT NULL COMMENT 'Define the scale type for this map, linear, logarithmic';
    
UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '2023_08_29_Map_scale_type.sql';
