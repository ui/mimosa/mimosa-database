INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('2023_09_20_Tidy_perms.sql', 'ONGOING');

SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE UserGroup_has_Person;
TRUNCATE TABLE UserGroup_has_Permission;
TRUNCATE TABLE Permission;
TRUNCATE TABLE UserGroup;

SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `UserGroup` (`userGroupId`, `name`) VALUES (1,'super_admin'),
(2,'bl_staff'),
(3,'id00_staff'),
(4,'spectroscopy_admin');

INSERT INTO `Permission` (`permissionId`, `type`, `description`) VALUES  (1,'manage_groups','Manage User Groups'),
(2,'manage_perms','Manage User Group Permissions'),
(3,'manage_persons','Manage User Group People'),
(4,'manage_options','Manage Application Options'),
(5,'view_activity','View Admin Audit Log'),
(6,'bl_staff','Staff Member for bl'),
(7,'id00_staff','Staff Member for id00'),
(8,'spec_admin','Admin for Spectroscopy');

INSERT INTO `UserGroup_has_Permission` (`userGroupId`, `permissionId`) VALUES (1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(2,6),
(3,7),
(4,8);


INSERT INTO `Person` (`personId`, `laboratoryId`, `siteId`, `personUUID`, `familyName`, `givenName`, `title`, `emailAddress`, `phoneNumber`, `login`, `faxNumber`, `recordTimeStamp`, `cache`, `externalId`) VALUES (3,NULL,NULL,NULL,'User','Super',NULL,NULL,NULL,'ijkl',NULL,'2023-07-26 13:08:20',NULL,NULL);

INSERT INTO `UserGroup_has_Person` (`userGroupId`, `personId`) VALUES (1,3),
(2,2),
(3,2),
(4,2);

INSERT INTO `AdminVar` (`name`, `value`) VALUES ('beamLineGroups', '[{\"groupName\": \"Spectroscopy\", \"uiGroup\": \"mapping\", \"permission\": \"spec_admin\", \"beamLines\": [{\"beamLineName\": \"bl\", \"archived\": false}, {\"beamLineName\": \"id00\", \"archived\": false}]}]');


UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '2023_09_20_Tidy_perms.sql';
