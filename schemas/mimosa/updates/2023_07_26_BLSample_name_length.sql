INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('2023_07_26_BLSample_name_length.sql', 'ONGOING');

ALTER TABLE `BLSample`
	MODIFY `name` varchar(100) DEFAULT NULL;

UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '2023_07_26_BLSample_name_length.sql';
