INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('2023_08_20_tidy2.sql', 'ONGOING');

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE `AutoProc`,
`AutoProcIntegration`,
`AutoProcScaling`,
`AutoProcScalingStatistics`,
`AutoProcScaling_has_Int`,
`AutoProcStatus`,
`CTF`,
`MotionCorrection`,
`Movie`,
`Screening`,
`ScreeningInput`,
`ScreeningOutput`,
`ScreeningOutputLattice`,
`ScreeningStrategy`,
`ScreeningStrategySubWedge`,
`ScreeningStrategyWedge`;

UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '2023_08_20_tidy2.sql';
