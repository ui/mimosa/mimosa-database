INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('2023_09_20_AdminVar_bump_version.sql', 'ONGOING');

UPDATE AdminVar SET `value` = '1.35.1' WHERE `name` = 'schemaVersion';

UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '2023_09_20_AdminVar_bump_version.sql';
