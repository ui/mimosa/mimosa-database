INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('2023_08_20_Sample_metadata2.sql', 'ONGOING');

ALTER TABLE `BLSubSample`
    CHANGE `metadata` `extraMetadata` JSON CHECK (JSON_VALID(extraMetadata)), ALGORITHM=INSTANT;

ALTER TABLE `BLSample`
    CHANGE `metadata` `extraMetadata` JSON CHECK (JSON_VALID(extraMetadata)), ALGORITHM=INSTANT;

UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '2023_08_20_Sample_metadata2.sql';
