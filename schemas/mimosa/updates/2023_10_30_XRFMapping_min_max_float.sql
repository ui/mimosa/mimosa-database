INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('2023_10_30_XRFMapping_min_max_float.sql', 'ONGOING');

ALTER TABLE `XRFFluorescenceMapping`
    CHANGE `min` `min` FLOAT NULL DEFAULT NULL COMMENT 'Min value in the data for histogramming',
    CHANGE `max` `max` FLOAT NULL DEFAULT NULL COMMENT 'Max value in the data for histogramming';

UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '2023_10_30_XRFMapping_min_max_float.sql';
