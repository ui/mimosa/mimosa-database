INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('2023_01_25_Sample_metadata.sql', 'ONGOING');

ALTER TABLE `BLSubSample`
	ADD metadata JSON CHECK (JSON_VALID(metadata)), ALGORITHM=INSTANT;

ALTER TABLE `BLSample`
	ADD metadata JSON CHECK (JSON_VALID(metadata)), ALGORITHM=INSTANT;

UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '2023_01_25_Sample_metadata.sql';
