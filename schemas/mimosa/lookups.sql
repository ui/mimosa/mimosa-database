
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*!40000 ALTER TABLE `AdminVar` DISABLE KEYS */;
INSERT INTO `AdminVar` (`varId`, `name`, `value`) VALUES (4,'schemaVersion','2.2.0'),
(5,'beamLineGroups','[{\"groupName\": \"Spectroscopy\", \"uiGroup\": \"mapping\", \"permission\": \"spec_admin\", \"beamLines\": [{\"beamLineName\": \"bl\", \"archived\": false}, {\"beamLineName\": \"id00\", \"archived\": false}]}]');
/*!40000 ALTER TABLE `AdminVar` ENABLE KEYS */;

/*!40000 ALTER TABLE `SchemaStatus` DISABLE KEYS */;
INSERT INTO `SchemaStatus` (`schemaStatusId`, `scriptName`, `schemaStatus`, `recordTimeStamp`) VALUES (6,'20180213_BLSample_subLocation.sql','DONE','2018-02-13 13:27:19'),
(12,'20180213_DataCollectionFileAttachment_fileType.sql','DONE','2018-02-13 15:12:54'),
(16,'20180303_v_run_to_table.sql','DONE','2018-07-25 15:11:18'),
(19,'20180328_ImageQualityIndicators_alter_table.sql','DONE','2018-07-25 15:11:18'),
(22,'20180410_BeamLineSetup_alter.sql','DONE','2018-07-25 15:11:18'),
(25,'20180413_BeamLineSetup_and_Detector_alter.sql','DONE','2018-07-25 15:11:18'),
(28,'20180501_DataCollectionGroup_experimentType_enum.sql','DONE','2018-07-25 15:11:18'),
(31,'20180531_ScreeningOutput_alignmentSuccess.sql','DONE','2018-07-25 15:11:18'),
(34,'20180629_DataCollection_imageContainerSubPath.sql','DONE','2018-07-25 15:11:18'),
(35,'20180913_BeamCalendar.sql','DONE','2018-09-19 09:52:45'),
(36,'2018_09_19_DataCollection_imageDirectory_comment.sql','DONE','2018-09-19 12:38:01'),
(37,'2018_09_27_increase_schema_version.sql','DONE','2018-09-27 13:17:15'),
(38,'2018_11_01_XrayCenteringResult.sql','DONE','2018-11-01 13:36:53'),
(39,'2018_11_01_AutoProcProgram_dataCollectionId.sql','DONE','2018-11-01 15:10:38'),
(40,'2018_11_01_AutoProcProgramMessage.sql','DONE','2018-11-01 15:28:17'),
(44,'2018_11_01_DiffractionPlan_centeringMethod.sql','DONE','2018-11-01 22:51:36'),
(45,'2018_11_02_DataCollectionGroup_experimentType_enum.sql','DONE','2018-11-02 11:54:15'),
(47,'2018_11_05_spelling_of_centring.sql','DONE','2018-11-05 15:31:38'),
(48,'2018_11_09_AutoProcProgram_update_processing_program.sql','DONE','2018-11-09 16:38:34'),
(49,'2018_11_14_AutoProcProgramMessage_autoinc.sql','DONE','2018-11-14 10:15:27'),
(50,'2018_11_22_AutoProcProgram_processingStatus_update.sql','DONE','2018-11-22 16:11:15'),
(51,'2018_12_04_EnergyScan_and_XFEFluorescenceSpectrum_add_axisPosition.sql','DONE','2018-12-04 14:13:23'),
(52,'2018_12_20_DataCollectionGroup_scanParameters.sql','DONE','2018-12-20 17:30:04'),
(53,'2019_01_14_Proposal_state.sql','DONE','2019-01-14 12:13:31'),
(54,'2019_01_14_ProcessingJobParameter_parameterValue.sql','DONE','2019-01-14 14:00:02'),
(57,'2019_01_15_Detector_localName.sql','DONE','2019-01-15 23:01:15'),
(58,'2019_02_04_BLSession_unique_index.sql','DONE','2019-02-04 13:52:19'),
(59,'2019_03_29_BLSession_archived.sql','DONE','2019-04-03 14:43:08'),
(60,'2019_04_03_UserGroup_and_Permission.sql','DONE','2019-04-03 14:51:04'),
(61,'2019_04_07_AdminVar_bump_version.sql','DONE','2019-04-07 11:35:06'),
(62,'2019_04_08_AdminVar_bump_version.sql','DONE','2019-04-08 15:38:01'),
(63,'2019_04_23_AdminVar_bump_version.sql','DONE','2019-04-23 11:13:27'),
(64,'2019_04_23_drop_v_run_view.sql','DONE','2019-04-23 11:13:35'),
(67,'2019_04_23_v_run_additional_runs.sql','DONE','2019-04-23 12:39:47'),
(68,'2019_05_28_AdminVar_bump_version.sql','DONE','2019-05-28 13:29:27'),
(72,'2019_07_17_BLSample_crystalId_default.sql','DONE','2019-07-17 15:21:59'),
(73,'2019_08_15_Sleeve.sql','DONE','2019-08-15 08:34:34'),
(74,'2019_08_15_AdminVar_bump_version.sql','DONE','2019-08-15 08:57:37'),
(75,'2019_08_28_AdminVar_bump_version.sql','DONE','2019-08-28 13:30:13'),
(76,'2019_08_30_AdminVar_bump_version.sql','DONE','2019-08-30 11:58:16'),
(77,'2019_10_06_BLSampleImage_fk3.sql','DONE','2019-10-06 16:55:44'),
(78,'2019_10_08_DiffractionPlan_experimentKind.sql','DONE','2019-10-08 12:47:10'),
(79,'2019_11_07_AutoProcProgramAttachment_importanceRank.sql','DONE','2019-11-07 16:35:25'),
(80,'2019_11_07_AdminVar_bump_version.sql','DONE','2019-11-07 16:45:44'),
(81,'2019_11_08_AdminVar_bump_version.sql','DONE','2019-11-08 16:09:52'),
(82,'2019_11_26_v_run_idx1.sql','DONE','2019-11-26 15:00:21'),
(83,'2019_12_02_AdminVar_bump_version.sql','DONE','2019-12-02 11:29:05'),
(84,'2019_12_02_AdminVar_bump_version_v2.sql','DONE','2019-12-02 18:14:11'),
(85,'2020_01_03_BLSampleImage_tables.sql','DONE','2020-01-03 16:05:45'),
(86,'2020_01_06_AdminVar_bump_version.sql','DONE','2020-01-06 11:45:02'),
(87,'2020_01_07_AdminVar_bump_version.sql','DONE','2020-01-07 09:45:25'),
(88,'2020_01_07_AdminVar_bump_version_v2.sql','DONE','2020-01-07 10:24:54'),
(89,'2020_01_07_AdminVar_bump_version_v3.sql','DONE','2020-01-07 11:16:09'),
(90,'2020_01_20_AdminVar_bump_version.sql','DONE','2020-01-20 13:40:52'),
(91,'2020_01_20_AdminVar_bump_version_v2.sql','DONE','2020-01-20 16:27:37'),
(92,'2020_02_13_SpaceGroup_data.sql','DONE','2020-02-13 16:52:53'),
(93,'2020_01_21_DiffractionPlan_experimentKind.sql','DONE','2020-02-13 17:13:17'),
(94,'2020_02_21_ProposalHasPerson_role_enum.sql','DONE','2020-02-21 14:36:10'),
(95,'2020_02_21_Session_has_Person_role_enum.sql','DONE','2020-02-21 14:36:17'),
(96,'2020_02_27_Container_scLocationUpdated.sql','DONE','2020-02-27 13:43:51'),
(97,'2020_03_09_Reprocessing_drop_tables.sql','DONE','2020-03-09 11:05:09'),
(98,'2020_03_24_ProcessingPipeline_tables.sql','DONE','2020-03-26 16:37:29'),
(99,'2020_03_25_ProcessingPipeline_ren_col.sql','DONE','2020-03-26 16:37:34'),
(100,'2020_03_27_AdminVar_bump_version.sql','DONE','2020-03-27 08:51:52'),
(101,'2020_03_27_AdminVar_bump_version_v2.sql','DONE','2020-03-27 15:07:56'),
(102,'2020_04_06_alterProtein.sql','DONE','2020-04-06 13:40:18'),
(103,'2020_04_27_BLSampleImageAutoScoreSchema_insert_CHIMP.sql','DONE','2020-04-27 14:37:41'),
(104,'2020_05_21_BLSampleImageAutoScoreClass_insert_CHIMP.sql','DONE','2020-05-21 17:52:54'),
(105,'2020_06_01_DewarRegistry_and_DewarRegistry_has_Proposal.sql','DONE','2020-06-01 10:29:19'),
(106,'2020_06_01_Protein_new_columns.sql','DONE','2020-06-01 10:29:32'),
(107,'2020_06_01_AdminVar_bump_version.sql','DONE','2020-06-01 10:46:11'),
(108,'2020_06_08_Shipping_comments.sql','DONE','2020-06-08 16:44:26'),
(109,'2020_06_10_DiffractionPlan_experimentKind.sql','DONE','2020-06-10 14:35:18'),
(110,'2020_06_15_Shipping_comments.sql','DONE','2020-06-15 14:01:25'),
(111,'2020_06_24_BLSampleGroup_name.sql','DONE','2020-06-24 10:56:25'),
(112,'2020_06_24_DiffractionPlan_userPath.sql','DONE','2020-06-24 10:56:30'),
(113,'2020_07_01_DewarRegistry_and_DewarRegistry_has_Proposal.sql','DONE','2020-07-01 13:51:49'),
(114,'2020_07_06_DewarRegistry_to_DewarRegistry_has_Proposal_data.sql','DONE','2020-07-06 10:59:22'),
(115,'2020_07_13_AdminVar_bump_version.sql','DONE','2020-07-13 18:14:39'),
(116,'2020_08_03_AdminVar_bump_version.sql','DONE','2020-08-03 15:19:36'),
(117,'2020_09_02_AutoProcScalingStatistics_new_index.sql','DONE','2020-09-02 17:02:33'),
(118,'2020_09_08_DewarRegistry_modify_fks.sql','DONE','2020-09-08 15:26:14'),
(119,'2020_08_28_ComponentSubType_changes.sql','DONE','2020-10-14 18:15:55'),
(120,'2020_08_28_ConcentrationType_changes.sql','DONE','2020-10-14 18:15:55'),
(121,'2020_08_28_Dewar_type.sql','DONE','2020-10-14 18:15:55'),
(122,'2020_08_28_DiffractionPlan_new_temperature_cols.sql','DONE','2020-10-14 18:15:55'),
(123,'2020_08_28_ExperimentType.sql','DONE','2020-10-14 18:15:55'),
(124,'2020_08_28_PurificationColumn.sql','DONE','2020-10-14 18:15:55'),
(125,'2020_08_29_BLSampleType.sql','DONE','2020-10-14 18:15:55'),
(126,'2020_08_29_Protein_isotropy.sql','DONE','2020-10-14 18:15:55'),
(127,'2020_10_16_AdminVar_bump_version.sql','DONE','2020-10-16 22:05:36'),
(128,'2020_10_19_AdminVar_bump_version.sql','DONE','2020-10-20 04:21:23'),
(129,'2020_10_22_GridInfo_dcId.sql','DONE','2020-11-09 13:57:27'),
(130,'2020_11_09_Phasing_method_enum.sql','DONE','2020-11-09 13:57:27'),
(133,'2020_11_09_AdminVar_bump_version.sql','DONE','2020-11-09 22:26:13'),
(134,'2020_11_10_SpaceGroup_update.sql','DONE','2020-11-20 17:49:46'),
(135,'2020_11_13_Dewar_facilityCode.sql','DONE','2020-11-20 17:49:46'),
(136,'2020_11_20_AdminVar_bump_version.sql','DONE','2020-11-20 17:49:46'),
(137,'2020_12_01_AdminVar_bump_version.sql','DONE','2020-12-01 12:21:43'),
(138,'2020_12_04_Container_experimentTypeId_FK.sql','DONE','2020-12-04 16:34:05'),
(139,'2020_12_04_AdminVar_bump_version.sql','DONE','2020-12-04 16:40:14'),
(140,'2020_11_22_diffractionplan_priority_and_mode.sql','DONE','2020-12-29 18:29:08'),
(141,'2020_12_07_AutoProc_index_unit_cell.sql','DONE','2020-12-29 18:29:08'),
(142,'2020_12_07_DataCollection_index_startTime.sql','DONE','2020-12-29 18:29:08'),
(143,'2020_12_10_BLSubSample_source.sql','DONE','2020-12-29 18:29:08'),
(144,'2020_12_30_AdminVar_bump_version.sql','DONE','2020-12-30 14:36:17'),
(145,'2021_01_13_AdminVar_bump_version.sql','DONE','2021-01-13 12:12:57'),
(146,'2021_01_14_AdminVar_bump_version.sql','DONE','2021-01-14 11:04:57'),
(147,'2020_07_31_add_offset_blsampleimage.sql','DONE','2021-02-22 12:28:16'),
(148,'2020_07_31_add_type_blsubsample.sql','DONE','2021-02-22 12:28:16'),
(149,'2020_07_31_extend_dcattachment_enum.sql','DONE','2021-02-22 12:28:16'),
(150,'2020_07_31_extend_dcg_type_enum.sql','DONE','2021-02-22 12:28:16'),
(151,'2020_07_31_extend_robotaction_enum.sql','DONE','2021-02-22 12:28:16'),
(152,'2020_07_31_refactor_xrfmapping.sql','DONE','2021-02-22 12:28:16'),
(153,'2020_11_22_blsample_staff_comments.sql','DONE','2021-02-22 12:28:16'),
(154,'2021_01_28_beamlinesetup_add_datacentre.sql','DONE','2021-02-22 12:28:16'),
(155,'2021_02_04_DiffractionPlan_strategyOption.sql','DONE','2021-02-22 12:28:16'),
(156,'2021_02_22_AdminVar_bump_version.sql','DONE','2021-02-22 13:06:57'),
(157,'2021_02_22_AdminVar_bump_version_v2.sql','DONE','2021-02-22 15:37:45'),
(158,'2020_08_28_ContainerType.sql','DONE','2021-03-05 16:09:40'),
(159,'2021_03_03_BF_automationError.sql','DONE','2021-03-05 16:09:41'),
(160,'2021_03_03_BF_automationFault.sql','DONE','2021-03-05 16:09:41'),
(161,'2021_03_03_cryoEMv2_0_tables.sql','DONE','2021-03-05 16:09:41'),
(162,'2021_03_05_AdminVar_bump_version.sql','DONE','2021-03-05 16:09:41'),
(163,'2021_03_05_ContainerType_update.sql','DONE','2021-04-13 15:50:39'),
(164,'2021_03_08_ContainerType_update.sql','DONE','2021-04-13 15:50:39'),
(165,'2021_03_09_SpaceGroup_update.sql','DONE','2021-04-13 15:50:39'),
(166,'2021_03_19_add_drop_indices.sql','DONE','2021-04-13 15:50:39'),
(167,'2021_03_19_ExperimentType_update.sql','DONE','2021-04-13 15:50:39'),
(168,'2021_04_01_BLSampleGroup_has_BLSample_modify_type.sql','DONE','2021-04-13 15:50:39'),
(169,'2021_04_01_ContainerType_insert.sql','DONE','2021-04-13 15:50:39'),
(170,'2021_04_12_cryoEMv2_1.sql','DONE','2021-04-13 15:50:39'),
(171,'2021_04_13_AdminVar_bump_version.sql','DONE','2021-04-13 16:17:12'),
(173,'2021_04_13_ContainerType_update.sql','DONE','2021-04-13 16:42:57'),
(174,'2021_04_20_AdminVar_bump_version.sql','DONE','2021-04-20 17:05:50'),
(175,'2020_11_19_ContainerQueueSample.sql','DONE','2021-05-14 16:07:45'),
(176,'2020_11_19_DataCollection.sql','DONE','2021-05-14 16:07:45'),
(177,'2021_04_23_Dewar_fk_constraint.sql','DONE','2021-05-14 16:07:46'),
(178,'2021_05_12_ParticleClassification_rotationAccuracy.sql','DONE','2021-05-14 16:07:46'),
(179,'2021_05_14_AdminVar_bump_version.sql','DONE','2021-05-14 16:21:50'),
(180,'2021_05_19_AdminVar_bump_version.sql','DONE','2021-05-19 16:01:54'),
(181,'2021_05_20_AdminVar_bump_version.sql','DONE','2021-05-20 10:30:35'),
(182,'2021_05_28_AdminVar_bump_version.sql','DONE','2021-05-28 15:46:50'),
(183,'2020_08_28_DiffractionPlan_new_cols.sql','DONE','2021-07-07 09:32:34'),
(184,'2021_06_01_BLSampleGroup_fk_proposalId.sql','DONE','2021-07-07 09:32:34'),
(185,'2021_06_09_DataCollectionGroup_experimentType_enum.sql','DONE','2021-07-07 09:32:34'),
(186,'2021_06_11_DataCollectionGroup_experimentType_enum.sql','DONE','2021-07-07 09:32:34'),
(187,'2021_06_17_SpaceGroup_update.sql','DONE','2021-07-07 09:32:34'),
(188,'2021_06_30_zc_ZocaloBuffer.sql','DONE','2021-07-07 09:32:34'),
(189,'2021_07_01_ParticleClassification_classDistribution.sql','DONE','2021-07-07 09:32:34'),
(190,'2021_07_01_ParticlePicker_summaryImageFullPath.sql','DONE','2021-07-07 09:32:34'),
(191,'2021_07_02_UserGroup_insert.sql','DONE','2021-07-07 09:32:34'),
(192,'2021_07_07_AdminVar_bump_version.sql','DONE','2021-07-07 10:35:39'),
(193,'2021_07_07_AdminVar_bump_version_v2.sql','DONE','2021-07-07 11:37:27'),
(194,'2021_07_08_AutoProcScalingStatistics_resIOverSigI2.sql','DONE','2021-07-23 17:36:44'),
(195,'2021_07_08_Screening_autoProcProgramId.sql','DONE','2021-07-23 17:36:45'),
(196,'2021_07_09_ProposalHasPerson_role_enum.sql','DONE','2021-07-23 17:36:45'),
(197,'2021_07_09_Session_has_Person_role_enum.sql','DONE','2021-07-23 17:36:45'),
(198,'2021_07_21_Positioner_tables.sql','DONE','2021-07-23 17:36:45'),
(199,'2021_07_23_AdminVar_bump_version.sql','DONE','2021-07-23 17:36:45'),
(200,'2021_07_23_AutoProcProgram_drop_dataCollectionId.sql','DONE','2021-07-23 17:36:45'),
(201,'2021_07_26_AdminVar_bump_version.sql','DONE','2021-07-26 10:41:54'),
(202,'2021_07_27_PDB_source.sql','DONE','2021-07-27 14:49:59'),
(204,'2021_07_28_AdminVar_bump_version.sql','DONE','2021-07-28 11:13:27'),
(205,'2021_06_18_ContainerType_update.sql','DONE','2021-08-31 10:04:06'),
(206,'2021_08_09_AdminVar_bump_version.sql','DONE','2021-08-31 10:04:06'),
(208,'2021_08_31_AdminVar_bump_version.sql','DONE','2021-08-31 10:30:39'),
(209,'2021_09_14_RelativeIceThickness.sql','DONE','2021-09-14 10:54:10'),
(210,'2021_09_15_AdminVar_bump_version.sql','DONE','2021-09-15 17:35:02'),
(211,'2021_08_05_MXMRRun_update.sql','DONE','2021-11-23 12:56:56'),
(212,'2021_09_23_BLSampleGroup_update_proposalId.sql','DONE','2021-11-23 12:56:56'),
(213,'2021_11_12_BLSampleImage_fullImagePath_idx.sql','DONE','2021-11-23 12:56:56'),
(214,'2021_11_23_AdminVar_bump_version.sql','DONE','2021-11-23 17:20:13'),
(215,'2022_01_12_Container_and_ContainerHistory_update.sql','DONE','2022-02-14 11:02:17'),
(216,'2022_01_20_Container_priorityPipelineId_default.sql','DONE','2022-02-14 11:02:18'),
(217,'2022_02_04_Pod.sql','DONE','2022-02-14 11:02:18'),
(218,'2022_02_14_AdminVar_bump_version.sql','DONE','2022-02-14 11:02:18'),
(219,'2022_03_07_SW_onceToken_recordTimeStamp_idx.sql','DONE','2022-06-22 12:09:07'),
(220,'2022_04_12_BLSession_fk_beamCalendarId_set_null.sql','DONE','2022-06-22 12:09:07'),
(221,'2022_05_12_Pod_app_modify_enum.sql','DONE','2022-06-22 12:09:07'),
(222,'2022_06_22_AdminVar_bump_version.sql','DONE','2022-06-22 12:09:07'),
(223,'2022_06_22_cryo-ET_tables.sql','DONE','2022-06-22 12:09:07'),
(224,'2022_07_17_BLSubSample_update_blSampleImageId.sql','DONE','2022-08-08 12:03:49'),
(225,'2022_08_08_AdminVar_bump_version.sql','DONE','2022-08-08 16:40:14'),
(226,'2022_06_28_diffractionplan_scanparameters.sql','DONE','2022-08-22 11:57:56'),
(227,'2022_06_28_gridinfo_patches.sql','DONE','2022-08-22 11:57:56'),
(228,'2022_06_28_sampleimage_positioner.sql','DONE','2022-08-22 11:57:56'),
(229,'2022_08_25_AdminVar_bump_version.sql','DONE','2022-08-25 15:55:14'),
(230,'2022_11_17_RobotActionPosition.sql','DONE','2023-07-26 12:43:59'),
(231,'2023_01_25_DataCollectionGroup_experimentType_enum.sql','DONE','2023-07-26 12:53:51'),
(232,'2023_07_26_AdminVar_bump_version.sql','DONE','2023-07-26 14:30:30'),
(233,'2023_07_26_BLSample_name_length.sql','DONE','2023-07-26 15:54:07'),
(234,'2023_01_25_Sample_metadata.sql','DONE','2023-08-21 13:13:17'),
(235,'2023_08_21_AdminVar_bump_version.sql','DONE','2023-08-21 13:27:32'),
(236,'2023_08_20_Sample_metadata2.sql','DONE','2023-08-21 14:19:14'),
(237,'2023_08_29_Map_scale_type.sql','DONE','2023-08-29 15:19:46'),
(238,'2023_08_30_AdminVar_bump_version.sql','DONE','2023-08-30 16:25:48'),
(239,'2023_09_20_Tidy_perms.sql','DONE','2023-09-20 08:54:19'),
(240,'2023_09_20_AdminVar_bump_version.sql','DONE','2023-09-20 09:13:58'),
(241,'2023_08_20_tidy.sql','DONE','2023-09-20 09:46:23'),
(242,'2023_10_30_AdminVar_bump_version.sql','DONE','2023-10-30 13:32:28'),
(243,'2023_10_30_XRFMapping_min_max_float.sql','DONE','2023-10-30 14:24:33'),
(244,'2023_10_31_0953_AdminVar_bump_version.sql','DONE','2023-10-31 10:02:34'),
(245,'2023_08_20_tidy2.sql','DONE','2024-09-19 12:29:05'),
(246,'2024_09_19_1252_AdminVar_bump_version.sql','DONE','2024-09-19 12:52:21'),
(247,'2024_09_19_1303_AdminVar_bump_version.sql','DONE','2024-09-19 13:04:05'),
(248,'2024_09_19_1331_AdminVar_bump_version.sql','DONE','2024-09-19 13:32:14');
/*!40000 ALTER TABLE `SchemaStatus` ENABLE KEYS */;

/*!40000 ALTER TABLE `ComponentType` DISABLE KEYS */;
INSERT INTO `ComponentType` (`componentTypeId`, `name`) VALUES (1,'Protein'),
(2,'DNA'),
(3,'Small Molecule'),
(4,'RNA');
/*!40000 ALTER TABLE `ComponentType` ENABLE KEYS */;

/*!40000 ALTER TABLE `ConcentrationType` DISABLE KEYS */;
INSERT INTO `ConcentrationType` (`concentrationTypeId`, `name`, `symbol`, `proposalType`, `active`) VALUES (1,'Molar','M',NULL,1),
(2,'Percentage Weight / Volume','%(w/v)',NULL,1),
(3,'Percentage Volume / Volume','%(v/v)',NULL,1),
(4,'Milligrams / Millilitre','mg/ml',NULL,1),
(5,'Grams','g',NULL,1),
(6,'Microlitre','uL','scm',1),
(7,'Millilitre','ml','scm',1);
/*!40000 ALTER TABLE `ConcentrationType` ENABLE KEYS */;

/*!40000 ALTER TABLE `ContainerType` DISABLE KEYS */;
INSERT INTO `ContainerType` (`containerTypeId`, `name`, `proposalType`, `active`, `capacity`, `wellPerRow`, `dropPerWellX`, `dropPerWellY`, `dropHeight`, `dropWidth`, `dropOffsetX`, `dropOffsetY`, `wellDrop`) VALUES (1,'B21_8+1','saxs',1,9,9,1,1,1,1,0,0,-1),
(2,'B21_96','saxs',1,192,12,2,1,0.5,1,0,0,-1),
(3,'B21_1tube','saxs',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(4,'I22_Capillary_Rack_20','saxs',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(5,'I22_Grid_100','saxs',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(6,'I22_Grid_45','saxs',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(7,'P38_Capillary_Rack_27','saxs',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(8,'P38_Solids','saxs',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(9,'P38_Powder','saxs',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(10,'B22_6','saxs',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(11,'I11_Capillary_Rack_6','saxs',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(12,'Puck','mx',1,16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(13,'ReferencePlate','mx',1,16,2,1,1,1,1,0,0,-1),
(14,'CrystalQuickX','mx',1,192,12,2,1,0.5,1,0,0,-1),
(15,'MitegenInSitu','mx',1,192,12,2,1,0.5,1,0,0,-1),
(16,'FilmBatch','mx',1,96,12,1,1,1,1,0,0,-1),
(17,'MitegenInSitu_3_Drop','mx',1,288,12,3,1,0.5,1,0,0,-1),
(18,'Greiner 3 Drop','mx',1,288,12,3,1,0.5,1,0,0,-1),
(19,'MRC Maxi','mx',1,48,6,1,1,1,0.5,0,0,-1),
(20,'MRC 2 Drop','mx',1,192,12,1,2,1,0.5,0.5,0,-1),
(21,'Griener 1536','mx',1,1536,12,4,4,1,1,0,0,-1),
(22,'3 Drop Square','mx',1,288,12,2,2,1,1,0,0,3),
(23,'SWISSCI 3 Drop','mx',1,288,12,2,2,1,1,0,0,1),
(24,'1 drop','mx',1,96,12,1,1,0.5,0.5,0,0,-1),
(25,'LCP Glass','mx',1,96,12,1,1,1,1,0,0,-1),
(26,'PCRStrip','saxs',1,9,9,1,1,1,1,0,0,-1),
(27,'Basket','mx',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(28,'Cane','mx',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(29,'Terasaki72','mx',0,72,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(30,'Puck-16','mx',1,16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(31,'Block-4','mx',1,16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(32,'Box','xpdf',1,25,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(33,'Puck-22','xpdf',1,22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(34,'I22_Grid_81','saxs',1,81,9,1,1,1,1,0,0,-1),
(35,'I22_Capillary_Rack_25','saxs',1,25,25,1,1,1,1,0,0,-1);
/*!40000 ALTER TABLE `ContainerType` ENABLE KEYS */;

/*!40000 ALTER TABLE `ExperimentType` DISABLE KEYS */;
INSERT INTO `ExperimentType` (`experimentTypeId`, `name`, `proposalType`, `active`) VALUES (1,'Default',NULL,1),
(2,'MXPressE','mx',1),
(3,'MXPressO','mx',1),
(4,'MXPressE_SAD','mx',1),
(5,'MXScore','mx',1),
(6,'MXPressM','mx',1),
(7,'MAD','mx',1),
(8,'SAD','mx',1),
(9,'Fixed','mx',1),
(10,'Ligand binding','mx',1),
(11,'Refinement','mx',1),
(12,'OSC','mx',1),
(13,'MAD - Inverse Beam','mx',1),
(14,'SAD - Inverse Beam','mx',1),
(15,'MESH','mx',1),
(16,'XFE','mx',1),
(17,'Stepped transmission','mx',1),
(18,'XChem High Symmetry','mx',1),
(19,'XChem Low Symmetry','mx',1),
(20,'Commissioning','mx',1),
(21,'HPLC','saxs',1),
(22,'Robot','saxs',1),
(23,'Rack','saxs',1),
(24,'Grid','saxs',1),
(25,'Solids','saxs',1),
(26,'Powder','saxs',1),
(27,'Peltier','saxs',1),
(28,'Spectroscopy','saxs',1),
(29,'CD Spectroscopy','saxs',1),
(30,'Microscopy','saxs',1),
(31,'Imaging','saxs',1),
(32,'CD Thermal Melt','saxs',1),
(33,'Fixed Energy At Ambient With Robot','saxs',1),
(34,'Mesh3D','mx',1),
(35,'Screening','sm',1),
(38,'XRD map','mapping',1),
(39,'XRF xrd map','mapping',1);
/*!40000 ALTER TABLE `ExperimentType` ENABLE KEYS */;

/*!40000 ALTER TABLE `InspectionType` DISABLE KEYS */;
INSERT INTO `InspectionType` (`inspectionTypeId`, `name`) VALUES (1,'Visible'),
(2,'UV');
/*!40000 ALTER TABLE `InspectionType` ENABLE KEYS */;

/*!40000 ALTER TABLE `WorkflowType` DISABLE KEYS */;
/*!40000 ALTER TABLE `WorkflowType` ENABLE KEYS */;

/*!40000 ALTER TABLE `Detector` DISABLE KEYS */;
INSERT INTO `Detector` (`detectorId`, `detectorType`, `detectorManufacturer`, `detectorModel`, `detectorPixelSizeHorizontal`, `detectorPixelSizeVertical`, `DETECTORMAXRESOLUTION`, `DETECTORMINRESOLUTION`, `detectorSerialNumber`, `detectorDistanceMin`, `detectorDistanceMax`, `trustedPixelValueRangeLower`, `trustedPixelValueRangeUpper`, `sensorThickness`, `overload`, `XGeoCorr`, `YGeoCorr`, `detectorMode`, `density`, `composition`, `numberOfPixelsX`, `numberOfPixelsY`, `detectorRollMin`, `detectorRollMax`, `localName`) VALUES (4,'Photon counting','In-house','Excalibur',NULL,NULL,NULL,NULL,'1109-434',100,300,NULL,NULL,NULL,NULL,NULL,NULL,NULL,55,'CrO3Br5Sr10',NULL,NULL,NULL,NULL,NULL),
(8,'Diamond XPDF detector',NULL,NULL,NULL,NULL,NULL,NULL,'1109-761',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10.4,'C+Br+He',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Detector` ENABLE KEYS */;

/*!40000 ALTER TABLE `UserGroup` DISABLE KEYS */;
INSERT INTO `UserGroup` (`userGroupId`, `name`) VALUES (2,'bl_staff'),
(3,'id00_staff'),
(4,'spectroscopy_admin'),
(1,'super_admin');
/*!40000 ALTER TABLE `UserGroup` ENABLE KEYS */;

/*!40000 ALTER TABLE `Permission` DISABLE KEYS */;
INSERT INTO `Permission` (`permissionId`, `type`, `description`) VALUES (1,'manage_groups','Manage User Groups'),
(2,'manage_perms','Manage User Group Permissions'),
(3,'manage_persons','Manage User Group People'),
(4,'manage_options','Manage Application Options'),
(5,'view_activity','View Admin Audit Log'),
(6,'bl_staff','Staff Member for bl'),
(7,'id00_staff','Staff Member for id00'),
(8,'spec_admin','Admin for Spectroscopy');
/*!40000 ALTER TABLE `Permission` ENABLE KEYS */;

/*!40000 ALTER TABLE `UserGroup_has_Permission` DISABLE KEYS */;
INSERT INTO `UserGroup_has_Permission` (`userGroupId`, `permissionId`) VALUES (1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(2,6),
(3,7),
(4,8);
/*!40000 ALTER TABLE `UserGroup_has_Permission` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

