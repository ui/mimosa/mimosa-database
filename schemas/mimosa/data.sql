
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*!40000 ALTER TABLE `AdminActivity` DISABLE KEYS */;
/*!40000 ALTER TABLE `AdminActivity` ENABLE KEYS */;

/*!40000 ALTER TABLE `AutoProcProgram` DISABLE KEYS */;
INSERT INTO `AutoProcProgram` (`autoProcProgramId`, `processingCommandLine`, `processingPrograms`, `processingStatus`, `processingMessage`, `processingStartTime`, `processingEndTime`, `processingEnvironment`, `recordTimeStamp`, `processingJobId`) VALUES (1,'test program (auto)','test program',NULL,NULL,NULL,NULL,NULL,'2020-12-28 18:12:16',1),
(2,'test program (auto)','test program',0,'processing failure','2020-12-28 18:21:52','2020-12-28 18:21:52',NULL,'2020-12-28 18:21:51',2),
(3,'test program (auto)','test program',1,'processing successful','2020-12-30 15:40:28','2020-12-30 15:40:33',NULL,'2020-12-30 15:40:27',3);
/*!40000 ALTER TABLE `AutoProcProgram` ENABLE KEYS */;

/*!40000 ALTER TABLE `AutoProcProgramAttachment` DISABLE KEYS */;
INSERT INTO `AutoProcProgramAttachment` (`autoProcProgramAttachmentId`, `autoProcProgramId`, `fileType`, `fileName`, `filePath`, `recordTimeStamp`, `importanceRank`) VALUES (1,3,'Result','moo.txt','/data',NULL,NULL);
/*!40000 ALTER TABLE `AutoProcProgramAttachment` ENABLE KEYS */;

/*!40000 ALTER TABLE `AutoProcProgramMessage` DISABLE KEYS */;
INSERT INTO `AutoProcProgramMessage` (`autoProcProgramMessageId`, `autoProcProgramId`, `recordTimeStamp`, `severity`, `message`, `description`) VALUES (1,3,'2021-01-14 16:12:23','WARNING','moo','a warning'),
(2,3,'2021-01-14 16:12:40','INFO','info','some info');
/*!40000 ALTER TABLE `AutoProcProgramMessage` ENABLE KEYS */;

/*!40000 ALTER TABLE `BLSample` DISABLE KEYS */;
INSERT INTO `BLSample` (`blSampleId`, `crystalId`, `containerId`, `name`, `code`, `location`, `comments`, `POSITIONID`, `recordTimeStamp`, `SMILES`, `volume`, `staffComments`, `extraMetadata`) VALUES (1,1,1,'sample1',NULL,'1',NULL,1,'2020-06-16 13:42:44',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `BLSample` ENABLE KEYS */;

/*!40000 ALTER TABLE `BLSampleImage` DISABLE KEYS */;
/*!40000 ALTER TABLE `BLSampleImage` ENABLE KEYS */;

/*!40000 ALTER TABLE `BLSampleImage_has_Positioner` DISABLE KEYS */;
/*!40000 ALTER TABLE `BLSampleImage_has_Positioner` ENABLE KEYS */;

/*!40000 ALTER TABLE `BLSample_has_DataCollectionPlan` DISABLE KEYS */;
/*!40000 ALTER TABLE `BLSample_has_DataCollectionPlan` ENABLE KEYS */;

/*!40000 ALTER TABLE `BLSample_has_Positioner` DISABLE KEYS */;
/*!40000 ALTER TABLE `BLSample_has_Positioner` ENABLE KEYS */;

/*!40000 ALTER TABLE `BLSession` DISABLE KEYS */;
INSERT INTO `BLSession` (`sessionId`, `beamLineSetupId`, `proposalId`, `beamCalendarId`, `startDate`, `endDate`, `beamLineName`, `scheduled`, `nbShifts`, `comments`, `beamLineOperator`, `bltimeStamp`, `visit_number`, `sessionTitle`, `externalId`, `archived`) VALUES (1,NULL,1,NULL,'2020-01-01 00:00:00','2030-01-01 23:59:59','bl',1,NULL,NULL,NULL,'2023-07-26 13:08:20',1,NULL,NULL,0),
(2,NULL,1,NULL,'2020-01-01 00:00:00','2030-01-01 23:59:59','id00',1,NULL,NULL,NULL,'2023-07-26 13:08:20',2,NULL,NULL,0);
/*!40000 ALTER TABLE `BLSession` ENABLE KEYS */;

/*!40000 ALTER TABLE `BLSubSample` DISABLE KEYS */;
INSERT INTO `BLSubSample` (`blSubSampleId`, `blSampleId`, `blSampleImageId`, `positionId`, `position2Id`, `comments`, `recordTimeStamp`, `source`, `type`, `extraMetadata`) VALUES (1,1,NULL,2,3,NULL,'2020-06-16 13:43:22','manual','roi',NULL),
(2,1,NULL,4,NULL,NULL,'2020-06-16 13:57:47','manual','poi',NULL),
(3,1,NULL,5,6,NULL,'2020-06-16 13:57:49','manual','loi',NULL);
/*!40000 ALTER TABLE `BLSubSample` ENABLE KEYS */;

/*!40000 ALTER TABLE `BLSubSample_has_Positioner` DISABLE KEYS */;
INSERT INTO `BLSubSample_has_Positioner` (`blSubSampleHasPositioner`, `blSubSampleId`, `positionerId`) VALUES (1,1,1),
(2,2,2),
(3,3,3);
/*!40000 ALTER TABLE `BLSubSample_has_Positioner` ENABLE KEYS */;

/*!40000 ALTER TABLE `BeamCalendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `BeamCalendar` ENABLE KEYS */;

/*!40000 ALTER TABLE `BeamLineSetup` DISABLE KEYS */;
/*!40000 ALTER TABLE `BeamLineSetup` ENABLE KEYS */;

/*!40000 ALTER TABLE `Container` DISABLE KEYS */;
INSERT INTO `Container` (`containerId`, `dewarId`, `code`, `containerType`, `capacity`, `sampleChangerLocation`, `containerStatus`, `bltimeStamp`, `beamlineLocation`, `screenId`, `barcode`, `comments`, `storageTemperature`, `containerTypeId`, `currentDewarId`) VALUES (1,1,'blc00001-1_Container1','Box',25,'1','processing','2020-06-16 15:42:44','bl',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Container` ENABLE KEYS */;

/*!40000 ALTER TABLE `ContainerHistory` DISABLE KEYS */;
INSERT INTO `ContainerHistory` (`containerHistoryId`, `containerId`, `location`, `blTimeStamp`, `status`, `beamlineName`, `currentDewarId`) VALUES (1,1,'1','2020-06-16 13:42:44','processing','bl',NULL);
/*!40000 ALTER TABLE `ContainerHistory` ENABLE KEYS */;

/*!40000 ALTER TABLE `ContainerInspection` DISABLE KEYS */;
INSERT INTO `ContainerInspection` (`containerInspectionId`, `containerId`, `inspectionTypeId`, `temperature`, `blTimeStamp`, `state`, `priority`, `manual`, `scheduledTimeStamp`, `completedTimeStamp`) VALUES (1,1,1,NULL,NULL,NULL,NULL,1,'2020-06-16 15:42:52','2020-06-16 15:42:52');
/*!40000 ALTER TABLE `ContainerInspection` ENABLE KEYS */;

/*!40000 ALTER TABLE `ContainerQueue` DISABLE KEYS */;
INSERT INTO `ContainerQueue` (`containerQueueId`, `containerId`, `personId`, `createdTimeStamp`, `completedTimeStamp`) VALUES (1,1,1,'2020-06-16 13:52:03',NULL);
/*!40000 ALTER TABLE `ContainerQueue` ENABLE KEYS */;

/*!40000 ALTER TABLE `ContainerQueueSample` DISABLE KEYS */;
INSERT INTO `ContainerQueueSample` (`containerQueueSampleId`, `containerQueueId`, `blSubSampleId`, `status`, `startTime`, `endTime`, `dataCollectionPlanId`, `blSampleId`) VALUES (1,1,1,NULL,NULL,NULL,5,NULL);
/*!40000 ALTER TABLE `ContainerQueueSample` ENABLE KEYS */;

/*!40000 ALTER TABLE `Crystal` DISABLE KEYS */;
INSERT INTO `Crystal` (`crystalId`, `proteinId`, `name`, `spaceGroup`, `morphology`, `color`, `size_X`, `size_Y`, `size_Z`, `cell_a`, `cell_b`, `cell_c`, `cell_alpha`, `cell_beta`, `cell_gamma`, `comments`, `recordTimeStamp`, `abundance`, `theoreticalDensity`) VALUES (1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-16 13:42:44',NULL,NULL);
/*!40000 ALTER TABLE `Crystal` ENABLE KEYS */;

/*!40000 ALTER TABLE `DataCollection` DISABLE KEYS */;
INSERT INTO `DataCollection` (`dataCollectionId`, `BLSAMPLEID`, `SESSIONID`, `experimenttype`, `dataCollectionNumber`, `startTime`, `endTime`, `runStatus`, `axisStart`, `axisEnd`, `axisRange`, `numberOfImages`, `numberOfPasses`, `exposureTime`, `imageDirectory`, `imagePrefix`, `imageSuffix`, `imageContainerSubPath`, `fileTemplate`, `wavelength`, `resolution`, `detectorDistance`, `xBeam`, `yBeam`, `comments`, `transmission`, `xtalSnapshotFullPath1`, `xtalSnapshotFullPath2`, `xtalSnapshotFullPath3`, `xtalSnapshotFullPath4`, `rotationAxis`, `beamSizeAtSampleX`, `beamSizeAtSampleY`, `dataCollectionGroupId`, `detectorId`, `flux`, `blSubSampleId`, `dataCollectionPlanId`) VALUES (1,1,0,NULL,2073510334,'2020-06-16 15:52:28','2020-06-16 15:53:17','Successful',NULL,NULL,NULL,16,NULL,0.1,'/data/bl/tmp/blc00001/sample1/sample1_roi1_1',NULL,NULL,'1.1/measurement','sample1_roi1_1.h5',1.23984,NULL,NULL,NULL,NULL,NULL,NULL,'/data/bl/tmp/blc00001/sample1/sample1_roi1_1/snapshot1_1592315556.5263455.png',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,1,5);
/*!40000 ALTER TABLE `DataCollection` ENABLE KEYS */;

/*!40000 ALTER TABLE `DataCollectionComment` DISABLE KEYS */;
/*!40000 ALTER TABLE `DataCollectionComment` ENABLE KEYS */;

/*!40000 ALTER TABLE `DataCollectionFileAttachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `DataCollectionFileAttachment` ENABLE KEYS */;

/*!40000 ALTER TABLE `DataCollectionGroup` DISABLE KEYS */;
INSERT INTO `DataCollectionGroup` (`dataCollectionGroupId`, `sessionId`, `comments`, `blSampleId`, `experimentType`, `startTime`, `endTime`, `workflowId`, `xtalSnapshotFullPath`, `scanParameters`, `experimentTypeId`) VALUES (1,1,NULL,NULL,'XRF map',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `DataCollectionGroup` ENABLE KEYS */;

/*!40000 ALTER TABLE `DataCollectionPlan_has_Detector` DISABLE KEYS */;
/*!40000 ALTER TABLE `DataCollectionPlan_has_Detector` ENABLE KEYS */;

/*!40000 ALTER TABLE `Dewar` DISABLE KEYS */;
INSERT INTO `Dewar` (`dewarId`, `shippingId`, `code`, `comments`, `storageLocation`, `dewarStatus`, `bltimeStamp`, `isStorageDewar`, `barCode`, `firstExperimentId`, `customsValue`, `transportValue`, `trackingNumberToSynchrotron`, `trackingNumberFromSynchrotron`, `type`, `facilityCode`, `weight`, `deliveryAgent_barcode`) VALUES (1,1,'blc00001-1_Dewar1',NULL,NULL,'processing',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'Dewar',NULL,NULL,NULL);
/*!40000 ALTER TABLE `Dewar` ENABLE KEYS */;

/*!40000 ALTER TABLE `DiffractionPlan` DISABLE KEYS */;
INSERT INTO `DiffractionPlan` (`diffractionPlanId`, `name`, `experimentKind`, `exposureTime`, `comments`, `recordTimeStamp`, `monochromator`, `energy`, `transmission`, `boxSizeX`, `boxSizeY`, `detectorId`, `monoBandwidth`, `userPath`, `experimentTypeId`, `collectionMode`, `priority`, `scanParameters`) VALUES (5,NULL,NULL,NULL,NULL,'2020-06-16 13:52:03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `DiffractionPlan` ENABLE KEYS */;

/*!40000 ALTER TABLE `EnergyScan` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnergyScan` ENABLE KEYS */;

/*!40000 ALTER TABLE `GridInfo` DISABLE KEYS */;
INSERT INTO `GridInfo` (`gridInfoId`, `xOffset`, `yOffset`, `dx_mm`, `dy_mm`, `steps_x`, `steps_y`, `meshAngle`, `recordTimeStamp`, `orientation`, `dataCollectionGroupId`, `pixelsPerMicronX`, `pixelsPerMicronY`, `snapshot_offsetXPixel`, `snapshot_offsetYPixel`, `snaked`, `dataCollectionId`, `patchesX`, `patchesY`) VALUES (1,NULL,NULL,0.05,0.05,4,4,NULL,'2020-06-16 13:52:36','horizontal',1,5.6338,-5.1338,482.058,562.012,0,1,1,1);
/*!40000 ALTER TABLE `GridInfo` ENABLE KEYS */;

/*!40000 ALTER TABLE `ImageQualityIndicators` DISABLE KEYS */;
/*!40000 ALTER TABLE `ImageQualityIndicators` ENABLE KEYS */;

/*!40000 ALTER TABLE `LabContact` DISABLE KEYS */;
/*!40000 ALTER TABLE `LabContact` ENABLE KEYS */;

/*!40000 ALTER TABLE `Laboratory` DISABLE KEYS */;
/*!40000 ALTER TABLE `Laboratory` ENABLE KEYS */;

/*!40000 ALTER TABLE `Person` DISABLE KEYS */;
INSERT INTO `Person` (`personId`, `laboratoryId`, `siteId`, `personUUID`, `familyName`, `givenName`, `title`, `emailAddress`, `phoneNumber`, `login`, `faxNumber`, `recordTimeStamp`, `cache`, `externalId`) VALUES (1,NULL,NULL,NULL,'User','Test',NULL,NULL,NULL,'abcd',NULL,'2023-07-26 13:08:20',NULL,NULL),
(2,NULL,NULL,NULL,'User','Admin',NULL,NULL,NULL,'efgh',NULL,'2023-07-26 13:08:20',NULL,NULL),
(3,NULL,NULL,NULL,'User','Super',NULL,NULL,NULL,'ijkl',NULL,'2023-07-26 13:08:20',NULL,NULL);
/*!40000 ALTER TABLE `Person` ENABLE KEYS */;

/*!40000 ALTER TABLE `Position` DISABLE KEYS */;
INSERT INTO `Position` (`positionId`, `relativePositionId`, `posX`, `posY`, `posZ`, `scale`, `recordTimeStamp`, `X`, `Y`, `Z`) VALUES (1,NULL,0,0,NULL,NULL,NULL,0,0,NULL),
(2,NULL,7410327,907061,NULL,NULL,NULL,7410327,907061,NULL),
(3,NULL,7610327,1107061,NULL,NULL,NULL,7610327,1107061,NULL),
(4,NULL,8331997,467086,NULL,NULL,NULL,8331997,467086,NULL),
(5,NULL,8613623,1668137,NULL,NULL,NULL,8613623,1668137,NULL),
(6,NULL,9077477,1311963,NULL,NULL,NULL,9077477,1311963,NULL);
/*!40000 ALTER TABLE `Position` ENABLE KEYS */;

/*!40000 ALTER TABLE `Positioner` DISABLE KEYS */;
INSERT INTO `Positioner` (`positionerId`, `positioner`, `value`) VALUES (1,'z',3),
(2,'z',3),
(3,'z',3);
/*!40000 ALTER TABLE `Positioner` ENABLE KEYS */;

/*!40000 ALTER TABLE `ProcessingJob` DISABLE KEYS */;
INSERT INTO `ProcessingJob` (`processingJobId`, `dataCollectionId`, `displayName`, `comments`, `recordTimestamp`, `recipe`, `automatic`) VALUES (1,1,NULL,NULL,'2020-12-28 18:12:16',NULL,0),
(2,1,NULL,NULL,'2020-12-28 18:21:51',NULL,1),
(3,1,NULL,NULL,'2020-12-30 15:40:27',NULL,1);
/*!40000 ALTER TABLE `ProcessingJob` ENABLE KEYS */;

/*!40000 ALTER TABLE `ProcessingJobImageSweep` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProcessingJobImageSweep` ENABLE KEYS */;

/*!40000 ALTER TABLE `ProcessingJobParameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProcessingJobParameter` ENABLE KEYS */;

/*!40000 ALTER TABLE `Proposal` DISABLE KEYS */;
INSERT INTO `Proposal` (`proposalId`, `personId`, `title`, `proposalCode`, `proposalNumber`, `bltimeStamp`, `proposalType`, `externalId`, `state`) VALUES (1,1,'Test Proposal','blc','00001','2023-07-26 13:08:20',NULL,NULL,'Open');
/*!40000 ALTER TABLE `Proposal` ENABLE KEYS */;

/*!40000 ALTER TABLE `ProposalHasPerson` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProposalHasPerson` ENABLE KEYS */;

/*!40000 ALTER TABLE `Protein` DISABLE KEYS */;
INSERT INTO `Protein` (`proteinId`, `proposalId`, `name`, `acronym`, `description`, `hazardGroup`, `containmentLevel`, `safetyLevel`, `molecularMass`, `bltimeStamp`, `sequence`, `componentTypeId`, `concentrationTypeId`, `externalId`, `density`, `abundance`, `isotropy`) VALUES (1,1,'Component 1','comp1',NULL,1,1,NULL,NULL,'2020-06-16 13:42:40',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Protein` ENABLE KEYS */;

/*!40000 ALTER TABLE `RobotAction` DISABLE KEYS */;
/*!40000 ALTER TABLE `RobotAction` ENABLE KEYS */;

/*!40000 ALTER TABLE `RobotActionPosition` DISABLE KEYS */;
/*!40000 ALTER TABLE `RobotActionPosition` ENABLE KEYS */;

/*!40000 ALTER TABLE `SW_onceToken` DISABLE KEYS */;
/*!40000 ALTER TABLE `SW_onceToken` ENABLE KEYS */;

/*!40000 ALTER TABLE `SessionType` DISABLE KEYS */;
/*!40000 ALTER TABLE `SessionType` ENABLE KEYS */;

/*!40000 ALTER TABLE `Session_has_Person` DISABLE KEYS */;
INSERT INTO `Session_has_Person` (`sessionId`, `personId`, `role`, `remote`) VALUES (1,1,NULL,0),
(2,1,NULL,0);
/*!40000 ALTER TABLE `Session_has_Person` ENABLE KEYS */;

/*!40000 ALTER TABLE `Shipping` DISABLE KEYS */;
INSERT INTO `Shipping` (`shippingId`, `proposalId`, `shippingName`, `deliveryAgent_agentName`, `deliveryAgent_shippingDate`, `deliveryAgent_deliveryDate`, `deliveryAgent_agentCode`, `deliveryAgent_flightCode`, `shippingStatus`, `bltimeStamp`, `laboratoryId`, `isStorageShipping`, `creationDate`, `comments`, `sendingLabContactId`, `returnLabContactId`, `returnCourier`, `dateOfShippingToUser`, `shippingType`, `SAFETYLEVEL`, `deliveryAgent_flightCodeTimestamp`, `deliveryAgent_label`, `readyByTime`, `closeTime`, `physicalLocation`, `deliveryAgent_pickupConfirmationTimestamp`, `deliveryAgent_pickupConfirmation`, `deliveryAgent_readyByTime`, `deliveryAgent_callinTime`, `deliveryAgent_productcode`, `deliveryAgent_flightCodePersonId`) VALUES (1,1,'blc00001-1_Shipment1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2020-06-16 15:42:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Shipping` ENABLE KEYS */;

/*!40000 ALTER TABLE `UserGroup_has_Person` DISABLE KEYS */;
INSERT INTO `UserGroup_has_Person` (`userGroupId`, `personId`) VALUES (1,3),
(2,2),
(3,2),
(4,2);
/*!40000 ALTER TABLE `UserGroup_has_Person` ENABLE KEYS */;

/*!40000 ALTER TABLE `Workflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `Workflow` ENABLE KEYS */;

/*!40000 ALTER TABLE `WorkflowStep` DISABLE KEYS */;
/*!40000 ALTER TABLE `WorkflowStep` ENABLE KEYS */;

/*!40000 ALTER TABLE `XFEFluorescenceComposite` DISABLE KEYS */;
/*!40000 ALTER TABLE `XFEFluorescenceComposite` ENABLE KEYS */;

/*!40000 ALTER TABLE `XFEFluorescenceSpectrum` DISABLE KEYS */;
/*!40000 ALTER TABLE `XFEFluorescenceSpectrum` ENABLE KEYS */;

/*!40000 ALTER TABLE `XRFFluorescenceMapping` DISABLE KEYS */;
INSERT INTO `XRFFluorescenceMapping` (`xrfFluorescenceMappingId`, `xrfFluorescenceMappingROIId`, `gridInfoId`, `dataFormat`, `data`, `points`, `opacity`, `colourMap`, `min`, `max`, `autoProcProgramId`, `scale`) VALUES (1,1,1,'json+gzip','�\0���^��6�Q �\0b(|0\0\0\0',NULL,1,'viridis',NULL,NULL,NULL,NULL),
(2,2,1,'json+gzip','�\0���^�-��\r1W�\0U�Ru�5����H�8>퓠!t-����%��oL4���Ŵ�qd9/�i��a��c��N3�m��D(nD\n9�W�+0)���\Z��Ik���X�N�9�_#��@?�0�\ZtV=N�x�P�������G�굔�]й=�:\Z�����F\0\0',NULL,1,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `XRFFluorescenceMapping` ENABLE KEYS */;

/*!40000 ALTER TABLE `XRFFluorescenceMappingROI` DISABLE KEYS */;
INSERT INTO `XRFFluorescenceMappingROI` (`xrfFluorescenceMappingROIId`, `startEnergy`, `endEnergy`, `element`, `edge`, `r`, `g`, `b`, `blSampleId`, `scalar`) VALUES (1,6370,6440,'Fe','Ka',NULL,NULL,NULL,1,NULL),
(2,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'simu1:deadtime_det1');
/*!40000 ALTER TABLE `XRFFluorescenceMappingROI` ENABLE KEYS */;

/*!40000 ALTER TABLE `v_run` DISABLE KEYS */;
/*!40000 ALTER TABLE `v_run` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

