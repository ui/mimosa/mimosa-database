#!/usr/bin/env bash

# Script to create the database from the schema .sql files and identify any
# update .sql files that haven't been run. Also generate documentation from
# inline comments in the schema.

# Some code borrowed from Stefan Buck's gist file at:
# https://gist.github.com/stefanbuck/ce788fee19ab6eb0b4447a85fc99f447

# Author: Karl Levik

set -e
source bin/functions.sh

if [ -z "${DB}" ]
then
  DB="mimosa_build"
fi

echo "Dropping + creating build database"
mysql --defaults-file=.my.cnf -e "DROP DATABASE IF EXISTS $DB; CREATE DATABASE $DB; SET GLOBAL log_bin_trust_function_creators=ON;"

if [[ $? -eq 0 ]]
then
  mysql --defaults-file=.my.cnf -D $DB < schemas/mimosa/tables.sql
  mysql --defaults-file=.my.cnf -D $DB < schemas/mimosa/lookups.sql
  mysql --defaults-file=.my.cnf -D $DB < schemas/mimosa/data.sql
  mysql --defaults-file=.my.cnf -D $DB < grants/acquisition_role.sql
  mysql --defaults-file=.my.cnf -D $DB < grants/mimosa_web_role.sql
  mysql --defaults-file=.my.cnf -D $DB < grants/sidecar_role.sql
  mysql --defaults-file=.my.cnf -D $DB < grants/replicator_role.sql

  arr=$(bin/missed_updates.sh)

  if [ -n "$arr" ]; then
    echo "Running schemas/mimosa/updates/*.sql files that haven't yet been run:"
    for sql_file in ${arr[@]}; do
      echo "$sql_file"
      if ! grep -Fxq "INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('${sql_file}', 'ONGOING');" "schemas/mimosa/updates/${sql_file}"; then
        echo "** ${sql_file} does not match SchemaStatus.scriptName INSERT value **"
        echo "  Looking for: INSERT INTO SchemaStatus (scriptName, schemaStatus) VALUES ('${sql_file}', 'ONGOING');"
        exit 1
      fi
      if ! grep -Fxq "UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '${sql_file}';" "schemas/mimosa/updates/${sql_file}"; then
        echo "** ${sql_file} does not match SchemaStatus.scriptName UPDATE value **"
        echo "  Looking for: UPDATE SchemaStatus SET schemaStatus = 'DONE' WHERE scriptName = '${sql_file}';"        
        exit 1
      fi
      mysql --defaults-file=.my.cnf -D ${DB} < "schemas/mimosa/updates/${sql_file}"
    done
  else
    echo "No new schemas/mimosa/updates/*.sql files."
  fi

fi
