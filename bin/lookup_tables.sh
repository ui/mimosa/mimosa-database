LOOKUP_TABLES=(
AdminVar
SchemaStatus
ComponentType
ConcentrationType
ContainerType
ExperimentType
InspectionType
WorkflowType
Detector
UserGroup
Permission
UserGroup_has_Permission
)

LOOKUP_TABLES_STRING=''
for TABLE in "${LOOKUP_TABLES[@]}"
do :
    LOOKUP_TABLES_STRING+=" ${TABLE}"
done
