# mimosa-database

A database for data acquisition and real-time monitoring for [daiquiri](http://gitlab.esrf.fr/ui/daiquiri)

## Requirements

* Server and a client packages of MariaDB 10.3 or later.
* A Unix-like OS with `bash` shell.
* The pandoc package (only required to build documentation at the end of the `build.sh` script.)
* If binary logging is enabled in the DB system, then execute this before importing the test schema: `SET GLOBAL log_bin_trust_function_creators=ON;`

## Installation

1. Install MariaDB. See the Wiki for ideas on how to install in a particular environment.
2. Copy `.my.example.cnf` to `.my.cnf` and then edit that file to set the `user` and `password`, e.g. `user = root` and use the `password` you set when securing. Optionally, you can also set e.g. `host` and `port`.
3. In a test environment run the `build.sh` file. This creates the database schema and applies the grants as described in the "Schema" and "Grants" sections below.

### Schema

Tip: Execute `./build.sh` to create a development `mimosa_build` database and import all the schema and grants files into it.

Alternatively, do it manually:

Run this on the command-line to create a database and import the schema stored in the SQL files:

```bash
mysql -e "CREATE DATABASE mimosa"
mysql mimosa < schemas/mimosa/tables.sql
mysql mimosa < schemas/mimosa/lookups.sql
mysql mimosa < schemas/mimosa/data.sql
```

Note that the `data.sql` file contains test data, so is only useful in a development environment.

### Grants

Then apply the grants:

```bash
mysql mimosa < grants/acquisition_role.sql
mysql mimosa < grants/replicator_role.sql
mysql mimosa < grants/sidecar_role.sql
mysql mimosa < grants/mimosa_web_role.sql
```

Note that the grants files are based on roles, so to actually use these grants, you also need to create database users and grant the roles to them. This is described in the header section of the grant files.

### Miscellaneous Notes

In a development environment it might be useful to log all SQL errors. In MariaDB, you can install the [SQL Error Log Plugin](https://mariadb.com/kb/en/library/sql-error-log-plugin/) to get these logged to a file `sql_errors.log` inside your datadir. Run this from the mariadb command-line:

```
INSTALL SONAME 'sql_errlog';
```
You can verify that it's installed and activated with:

```
SHOW PLUGINS SONAME WHERE Name = 'SQL_ERROR_LOG';
```

## Updating

In order to update a production database, please follow this procedure:

For all *.sql files in `schemas/mimosa/updates` that have not already been run, read any comments inside the files to decide if/when you should run them. Run a file e.g. like this:
```bash
mysql mimosa < schemas/mimosa/updates/2019_03_29_BLSession_archived.sql
```

## Useful scripts

* `build.sh`: Creates a development `mimosa_build` database and imports all the schema and grants files into it.
* `bin/backup.sh`: Makes a backup of the development database.
* `bin/missed_updates.sh`: Generates a list of files in the `schemas/mimosa/updates/` folder that haven't been applied.

## Documentation

[https://dbdocs.io/mimosa/mimosa](https://dbdocs.io/mimosa/mimosa)
