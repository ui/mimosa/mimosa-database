-- Create the web application role.
CREATE ROLE IF NOT EXISTS mimosa_web;

-- You must also create a database user and grant this role to them, e.g.
-- CREATE USER mimosa@'%' IDENTIFIED BY 'the_mimosa_password';
-- GRANT mimosa_web to mimosa@'%';
-- SET DEFAULT ROLE mimosa_web FOR mimosa@'%';

-- The grants for the web application role:
GRANT SELECT ON * TO 'mimosa_web';
GRANT SELECT, UPDATE, INSERT ON BLSession TO 'mimosa_web';
GRANT SELECT, INSERT ON SessionType TO 'mimosa_web';
GRANT INSERT, UPDATE ON DataCollection TO 'mimosa_web';
GRANT INSERT ON DataCollectionGroup TO 'mimosa_web';
GRANT INSERT ON ProcessingJob TO 'mimosa_web';
GRANT INSERT ON ProcessingJobParameter TO 'mimosa_web';
GRANT SELECT, UPDATE ON EnergyScan TO 'mimosa_web';
GRANT SELECT, UPDATE ON XFEFluorescenceSpectrum TO 'mimosa_web';
GRANT UPDATE ON DataCollectionGroup TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON DataCollectionComment TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON Shipping TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON Dewar TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON `Container` TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON ContainerHistory TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON ContainerQueue TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON ContainerQueueSample TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON BLSample TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON BLSubSample TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON Crystal TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON Protein TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON DiffractionPlan TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON LabContact TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON Person TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON Laboratory TO 'mimosa_web';

GRANT INSERT, UPDATE, DELETE ON UserGroup_has_Permission TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON Permission TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON UserGroup_has_Person TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON `UserGroup` TO 'mimosa_web';

GRANT INSERT, UPDATE, DELETE ON ConcentrationType TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON ComponentType TO 'mimosa_web';

GRANT INSERT, UPDATE, DELETE ON AdminActivity TO 'mimosa_web';
GRANT INSERT, UPDATE, DELETE ON AdminVar TO 'mimosa_web';

GRANT INSERT, UPDATE, DELETE ON ContainerInspection TO 'mimosa_web';

GRANT INSERT, UPDATE, DELETE ON BLSampleImage TO 'mimosa_web';

GRANT SELECT, INSERT, UPDATE, DELETE ON SW_onceToken TO 'mimosa_web';

GRANT SELECT, INSERT, UPDATE, DELETE ON BLSample_has_DataCollectionPlan TO 'mimosa_web';
GRANT SELECT, INSERT, UPDATE ON Detector TO 'mimosa_web';
GRANT SELECT, INSERT, UPDATE, DELETE ON DataCollectionPlan_has_Detector TO 'mimosa_web';

GRANT SELECT, INSERT, UPDATE, DELETE ON XRFFluorescenceMappingROI TO 'mimosa_web';
GRANT SELECT, INSERT, UPDATE, DELETE ON XRFFluorescenceMapping TO 'mimosa_web';
GRANT SELECT, INSERT, UPDATE, DELETE ON XFEFluorescenceComposite TO 'mimosa_web';

GRANT SELECT, INSERT ON Session_has_Person TO 'mimosa_web';
