-- Create the web application role.
CREATE ROLE IF NOT EXISTS replicator;

-- You must also create a database user and grant this role to them, e.g.
-- CREATE USER replicator@'%' IDENTIFIED BY 'replicator_password';
-- GRANT replicator to replicator@'%';
-- SET DEFAULT ROLE replicator FOR replicator@'%';

-- The grants for the replicator role:
GRANT SELECT ON Detector TO 'replicator';
GRANT SELECT ON BeamCalendar TO 'replicator';
GRANT SELECT ON ComponentType TO 'replicator';
GRANT SELECT ON ConcentrationType TO 'replicator';
GRANT SELECT ON Laboratory TO 'replicator';
GRANT SELECT, UPDATE, INSERT ON BLSession TO 'replicator';
GRANT SELECT, UPDATE, INSERT ON Session_has_Person TO 'replicator';
GRANT SELECT, UPDATE, INSERT ON Proposal TO 'replicator';
GRANT SELECT, INSERT ON SessionType TO 'replicator';
GRANT SELECT, INSERT, UPDATE ON Protein TO 'replicator';
GRANT SELECT, INSERT, UPDATE ON Person TO 'replicator';

GRANT SELECT, INSERT, UPDATE ON UserGroup_has_Permission TO 'replicator';
GRANT SELECT, INSERT, UPDATE ON Permission TO 'replicator';
GRANT SELECT, INSERT, UPDATE ON UserGroup_has_Person TO 'replicator';
GRANT SELECT, INSERT, UPDATE ON `UserGroup` TO 'replicator';
