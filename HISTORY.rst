=======
History
=======

Unreleased / master
-------------------

2.2.0
-----
Final renaming

2.1.0
-----
Further tidying and cleanup

2.0.0
-----

Initial minimal fork
